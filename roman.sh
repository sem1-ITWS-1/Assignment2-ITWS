#!/bin/bash
n=$1
if [ $n -ge 100 ]
then
	while [ $n -ge 100 ]
	do
		printf "C"
		n=$((n-100))
	done
fi
if [ $n -ge 90 ]
then
	printf "XC"
	n=$((n-90))
fi
if [ $n -ge 50 ]
then
	printf "L"
	n=$((n-50))
fi
if [ $n -ge 40 ]
then
	printf "XL"
	n=$((n-40))
fi
if [ $n -ge 10 ]
then
	while [ $n -ge 10 ]
	do
		printf "X"
		n=$((n-10))
	done
fi

if [ $n -ge 9 ]
then
	printf "IX"
	n=$((n-10))
fi
if [ $n -ge 5 ]
then
	printf "V"
	n=$((n-5))
fi
if [ $n -ge 4 ]
then
	printf "IV"
	n=$((n-4))
fi
if [ $n -ge 1 ]
then
	while [ $n -ge 1 ]
	do
		printf "I"
		n=$((n-1))
	done
fi




