#!/bin/bash
if [ "$#" != "1" ]
then
	echo error : Enter one file name.
	exit
fi
if [ -f $1 ]
then
	chmod u+x $1
else
	echo error : Please enter a valid file name.
fi


