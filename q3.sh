#!/bin/bash
flag=0
for i in `find *.txt`
do
	count=`grep -c "$1" $i`
	if [ "$count" != "0" ]
	then
		echo "$count lines in $i"
		flag=1
	fi
done
if [ $flag -eq 0 ]
then
	false
fi
