#!/bin/bash
num=`df -h|grep "dev/sda"|wc -l` 
echo $num
for (( i=1; i<=$num; i++ ))
do
	disk=`df -h|grep dev/sda|sed $i!d|cut -d ' ' -f1`
	space=`df -h|grep dev/sda|sed $i!d|tr -s ' '|cut -d ' ' -f5`
        spacenum=${space//%}
	if [ $spacenum -lt $1 ]
	then
		echo "OK, $disk, $space"
	elif [ $spacenum -gt $2 ]
	then
		echo "WARNING, $disk, $space"
	else
		echo "CRITICAL, $disk, $space"
	fi
done
