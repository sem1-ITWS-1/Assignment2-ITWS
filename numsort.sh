#!/bin/bash
declare -a arr
arr=("$@")
for (( i=0; i<$#; i++ ))
do
	for (( j=$i; j<$#; j++ ))
	do
		if [ ${arr[$i]} -gt ${arr[$j]} ]
		then
			temp=${arr[$i]}
			arr[$i]=${arr[$j]}
			arr[$j]=$temp
			
		fi
	done
done
for (( i=0; i<$#; i++ ))
do 
	printf "${arr[$i]} "
done
